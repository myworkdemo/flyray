package me.flyray.erp.api;

import java.util.List;

import me.flyray.erp.model.User;

public interface UserService {

	public List<User> displayAllUser();
	
}
